﻿using BugTracker.Infrastructure.Models;
using System.Collections.Generic;
using System.Linq;

namespace BugTracker.Infrastructure.Repositories
{
    public class UserRepository : Repository
    {
        public UserRepository(ApplicationDbContext applicationDbContext) 
            : base(applicationDbContext) {}

        public List<User> Get(int? id)
        {
            List<User> users;

            // Get the specified user
            if (id != null)
            {
                users = Db.Users
                    .Where(x => x.Id == id)?
                    .ToList();
            }
            // Get all the users
            else
            {
                users = Db.Users?.ToList();
            }

            return users;
        }

        public User Update(User userInput)
        {
            // Get the existing user
            User user = Db.Users.Find(userInput.Id);

            if (user != null)
            {
                user.Name = userInput.Name;
                user.Email = userInput.Email;

                Db.Users.Update(user);
                Db.SaveChanges();
            }

            return user;
        }

        public User Add(string name, string email)
        {
            // Create a new user record from the values supplied
            User user = new User
            {
                Name = name,
                Email = email
            };

            Db.Users.Add(user);
            Db.SaveChanges();

            return user;
        }

        public User Delete(int id)
        {
            User user = Db.Users.Find(id);
            Db.Users.Remove(user);
            Db.SaveChanges();

            return user;
        }
    }
}
