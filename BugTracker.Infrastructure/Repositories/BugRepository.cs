﻿using BugTracker.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BugTracker.Infrastructure.Repositories
{
    public class BugRepository : Repository
    {
        public BugRepository(ApplicationDbContext applicationDbContext) 
            : base(applicationDbContext) {}

        public List<Bug> Get(int? id)
        {
            List<Bug> bugs;

            // Get the specified bug
            if (id != null)
            {
                bugs = Db.Bugs
                    .Include(x => x.Assignee)
                    .Include(x => x.Status)
                    .Where(x => x.Id == id)?
                    .ToList();
            }
            // Get all the bugs
            else
            {
                bugs = Db.Bugs
                    .Include(x => x.Assignee)
                    .Include(x => x.Status)?
                    .ToList();
            }

            return bugs;
        }

        public Bug Add(string title, string description, int assigneeId,
            int statusId)
        {
            // Create a new bug record from the values supplied
            Bug bug = new Bug
            {
                Title = title,
                Description = description,
                Assignee = Db.Users.Find(assigneeId),
                Created = DateTime.Now,
                Status = Db.Statuses.Find(statusId)
            };

            Db.Bugs.Add(bug);
            Db.SaveChanges();

            return bug;
        }

        public Bug Update(Bug bugInput)
        {
            // Get the existing bug
            Bug bug = Db.Bugs.Find(bugInput.Id);

            if (bug != null)
            {
                bug.Title = bugInput.Title;
                bug.Description = bugInput.Description;
                bug.Assignee = Db.Users.Find(bugInput.AssigneeId);
                bug.Status = Db.Statuses.Find(bugInput.StatusId);

                Db.Bugs.Update(bug);
                Db.SaveChanges();
            }

            return bug;
        }

        public Bug Delete(int id)
        {
            Bug bug = Db.Bugs.Find(id);
            Db.Bugs.Remove(bug);
            Db.SaveChanges();

            return bug;
        }
    }
}
