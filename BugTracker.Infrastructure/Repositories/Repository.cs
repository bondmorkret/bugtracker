﻿using BugTracker.Infrastructure.Models;

namespace BugTracker.Infrastructure.Repositories
{
    public abstract class Repository
    {
        public Repository(ApplicationDbContext applicationDbContext)
        {
            Db = applicationDbContext;
        }

        protected readonly ApplicationDbContext Db;
    }
}
