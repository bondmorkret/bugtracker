﻿using BugTracker.Infrastructure.Models;
using System.Collections.Generic;
using System.Linq;

namespace BugTracker.Infrastructure.Repositories
{
    public class StatusRepository : Repository
    {
        public StatusRepository(ApplicationDbContext applicationDbContext) 
            : base(applicationDbContext) {}

        public List<Status> Get()
        {
            // Get all the statuses
            return Db.Statuses?.ToList();
        }

        public Status Add(string name)
        {
            // Create a new status record from the values supplied
            Status status = new Status
            {
                Name = name
            };

            Db.Statuses.Add(status);
            Db.SaveChanges();

            return status;
        }

        public Status Delete(int id)
        {
            Status status = Db.Statuses.Find(id);
            Db.Statuses.Remove(status);
            Db.SaveChanges();

            return status;
        }
    }
}
