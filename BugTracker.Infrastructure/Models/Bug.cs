﻿using System;

namespace BugTracker.Infrastructure.Models
{
    public class Bug
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime Created { get; set; }
        public int StatusId { get; set; }
        public Status Status { get; set; }
        public int? AssigneeId { get; set; }
        public User Assignee { get; set; }
    }
}
