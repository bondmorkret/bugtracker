﻿export class CreateBug {
    name: string;
    description: string;
    assigneeId: number;
    statusId: number;
}