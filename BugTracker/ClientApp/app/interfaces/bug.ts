﻿import { User } from './user';
import { Status } from './status';

export interface Bug {
    id: number;
    title: string;
    description: string;
    created: Date;
    statusId: number;
    status: Status;
    assigneeId: number;
    assignee: User;
}