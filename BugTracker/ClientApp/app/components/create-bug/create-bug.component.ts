import { Component, Inject } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from "@angular/router";
import { User } from '../../interfaces/user';
import { Status } from '../../interfaces/status';
import { CreateBug } from '../../models/create-bug';
import { Bug } from '../../interfaces/bug';

@Component({
    selector: 'create-bug',
    templateUrl: './create-bug.component.html'
})
export class CreateBugComponent {

    constructor(
        private http: Http,
        private router: Router
    ) {}

    public statuses: Status[];
    public users: User[];
    public model: CreateBug = new CreateBug();

    ngOnInit() {
        // Call the API to get the statuses
        this.http.get('api/status').subscribe(result => {
            this.statuses = result.json() as Status[];
        }, error => console.error(error));

        // Call the API to get the users
        this.http.get('api/user').subscribe(result => {
            this.users = result.json() as User[];
        }, error => console.error(error));
    }

    onSubmit() {
        // Call the API to create the bug
        this.http.post('api/bug', this.model).subscribe(result => {
            let bug = result.json() as Bug;
            this.router.navigate(['/detail/', bug.id]);
        }, error => console.error(error));
    }
}