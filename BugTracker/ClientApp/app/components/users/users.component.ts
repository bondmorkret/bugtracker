import { Component, Inject } from '@angular/core';
import { Http } from '@angular/http';
import { User } from '../../interfaces/user';

@Component({
    selector: 'users',
    templateUrl: './users.component.html'
})
export class UsersComponent {

    constructor(
        private http: Http,
        @Inject('BASE_URL') private baseUrl: string
    )
    {
        this.getUsers();
    }

    public users: User[];
    public editing: User | undefined;

    getUsers() {
        this.http.get(this.baseUrl + 'api/user').subscribe(result => {
            this.users = result.json() as User[];
        }, error => console.error(error));
    }

    delete(user: User) {
        if (confirm("Are you sure you want to delete '" + user.name + "'?")) {
            // Call the API to delete the user
            this.http.delete('api/user?id=' + user.id).subscribe(result => {
                this.getUsers();
            }, error => console.error(error));
        }
    }

    save(user: User) {
        // Call the API to update the user
        this.http.patch('api/user', user).subscribe(result => {
            this.getUsers();
        }, error => console.error(error));
    }

    editUser(user: User, $event: Event) {
        $event.preventDefault();
        $event.stopPropagation();
        this.editing = user;
    }
}
