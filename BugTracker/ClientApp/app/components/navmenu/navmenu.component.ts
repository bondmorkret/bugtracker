import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { CreateUser } from '../../models/create-user';
import { CreateStatus } from '../../models/create-status';

@Component({
    selector: 'nav-menu',
    templateUrl: './navmenu.component.html',
    styleUrls: ['./navmenu.component.css']
})
export class NavMenuComponent {

    constructor(
        private http: Http
    ) {}

    public quickCreateStatus: boolean = false;
    public quickCreateUser: boolean = false;
    public user: CreateUser = new CreateUser();
    public status: CreateStatus = new CreateStatus();

    openQuickCreate(type: string) {
        if (type === 'user') {
            this.quickCreateStatus = false;
            this.quickCreateUser = true;
        }
        if (type === 'status') {
            this.quickCreateStatus = true;
            this.quickCreateUser = false;
        }
    }
    closeQuickCreate() {
        this.quickCreateStatus = false;
        this.quickCreateUser = false;
    }

    saveUser() {
        // Call the API to create the user
        this.http.post('api/user', this.user).subscribe(result => {
            this.closeQuickCreate();
        }, error => console.error(error));
    }

    saveStatus() {
        // Call the API to create the status
        this.http.post('api/status', this.status).subscribe(result => {
            this.closeQuickCreate();
        }, error => console.error(error));
    }
}
