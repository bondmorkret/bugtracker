import { Component, OnInit, Inject, HostListener } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Http } from '@angular/http';
import { switchMap } from 'rxjs/operators';
import { Bug } from '../../interfaces/bug';
import { User } from '../../interfaces/user';
import { Status } from '../../interfaces/status';

@Component({
    selector: 'detail',
    templateUrl: './detail.component.html'
})
export class DetailComponent {

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private http: Http
    ) {}

    public id: number;
    public bug: Bug;
    public editing: string | undefined;
    public edited: boolean = false;
    public assigneeEdited: boolean = false;
    public statuses: Status[];
    public users: User[];

    ngOnInit() {
        // Get the bug id from the routing
        this.id = parseInt(this.route.snapshot.paramMap.get('id') || '');
        console.log(this.id);

        // Call the API to get the bug
        this.http.get('api/bug?id=' + this.id).subscribe(result => {
            this.bug = result.json() as Bug;
        }, error => console.error(error));
    }

    edit(propertyName: string, $event: Event) {
        $event.preventDefault();
        $event.stopPropagation();
        this.editing = propertyName;
        this.edited = true;

        // Get the statuses and users if they haven't already been retrieved
        if (propertyName === 'status' || propertyName === 'assignee') {
            if (!this.statuses) {
                // Call the API to get the statuses
                this.http.get('api/status').subscribe(result => {
                    this.statuses = result.json() as Status[];
                }, error => console.error(error));
            }
            if (!this.users) {
                // Call the API to get the users
                this.http.get('api/user').subscribe(result => {
                    this.users = result.json() as User[];
                }, error => console.error(error));
            }

            if (propertyName === 'assignee') {
                this.assigneeEdited = true;
            }
        }
    }

    @HostListener('document:click', ['$event']) clickedOutside($event: Event) {
        this.editing = undefined
    }

    save(bug: Bug) {
        this.http.patch('api/bug', bug).subscribe(result => {
            this.bug = result.json() as Bug;
            this.editing = undefined;
            this.edited = false;
            this.assigneeEdited = false;
        }, error => console.error(error));
    }

    updateStatus(status: Status) {
        this.bug.status = status;
        this.bug.statusId = status.id;
        this.editing = undefined;
    }

    updateAssignee(user: User) {
        this.bug.assignee = user;
        this.bug.assigneeId = user.id;
        this.assigneeEdited = true;
    }

    getAssigneeName() {
        return this.bug.assignee != null ? this.bug.assignee.name : 'Unassigned';
    }
}
