import { Component, Inject } from '@angular/core';
import { Http } from '@angular/http';
import { Bug } from '../../interfaces/bug';

@Component({
    selector: 'home',
    templateUrl: './home.component.html'
})
export class HomeComponent {
    public bugs: Bug[]
    public hoveringBug: Bug | undefined;

    constructor(
        private http: Http,
        @Inject('BASE_URL') private baseUrl: string
    )
    {
        this.getBugs();
    }

    getBugs() {
        this.http.get(this.baseUrl + 'api/bug').subscribe(result => {
            this.bugs = result.json() as Bug[];
        }, error => console.error(error));
    }

    showOptions(bug: Bug) {
        this.hoveringBug = bug;
    }

    hideOptions() {
        this.hoveringBug = undefined;
    }

    delete(bug: Bug) {
        if (confirm("Are you sure you want to delete '" + bug.title + "'?")) {
            // Call the API to get the users
            this.http.delete('api/bug?id=' + bug.id).subscribe(result => {
                this.getBugs();
            }, error => console.error(error));
        }
    }
}
