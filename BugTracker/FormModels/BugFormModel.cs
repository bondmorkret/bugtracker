﻿namespace BugTracker.Web.FormModels
{
    public class BugFormModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int AssigneeId { get; set; }
        public int StatusId { get; set; }
    }
}
