﻿namespace BugTracker.Web.FormModels
{
    public class UserFormModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
    }
}
