﻿using BugTracker.Infrastructure.Models;
using BugTracker.Infrastructure.Repositories;
using BugTracker.Web.FormModels;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace BugTracker.Web.Controllers
{
    public class BugApiController : Controller
    {
        public BugApiController(ApplicationDbContext dbContext)
        {
            _bugRepository = new BugRepository(dbContext);
        }

        private readonly BugRepository _bugRepository;

        [HttpGet]
        [Route("/api/bug")]
        public ActionResult Get(int? id)
        {
            // Use the bugrepository to retrieve the bug/s from the DB
            List<Bug> bugs = _bugRepository.Get(id);

            if (bugs.Any())
            {
                if (id != null)
                {
                    return Json(bugs.First());
                }
                return Json(bugs);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpPost]
        [Route("/api/bug")]
        public ActionResult Add([FromBody]BugFormModel formModel)
        {
            // Use the bugrepository to store the bug in the DB
            Bug bug = _bugRepository.Add(formModel.Title,
                                         formModel.Description,
                                         formModel.AssigneeId,
                                         formModel.StatusId);

            return Json(bug);
        }

        [HttpDelete]
        [Route("/api/bug")]
        public ActionResult Delete(int id)
        {
            // Use the bugrepository to remove the bug from the DB
            Bug bug = _bugRepository.Delete(id);

            return Json(bug);
        }

        [HttpPatch]
        [Route("/api/bug")]
        public ActionResult Update([FromBody]Bug bugInput)
        {
            // Use the bugrepository to update the bug in the DB
            Bug bug = _bugRepository.Update(bugInput);

            if (bug != null)
            {
                return Json(bug);
            }
            else
            {
                return NotFound();
            }
        }
    }
}
