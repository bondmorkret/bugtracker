﻿using BugTracker.Infrastructure.Models;
using BugTracker.Infrastructure.Repositories;
using BugTracker.Web.FormModels;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace BugTracker.Web.Controllers
{
    public class StatusApiController : Controller
    {
        public StatusApiController(ApplicationDbContext dbContext)
        {
            _statusRepository = new StatusRepository(dbContext);
        }

        private readonly StatusRepository _statusRepository;

        [HttpGet]
        [Route("/api/status")]
        public ActionResult Get()
        {
            // Use the statusrepository to retrieve the statuses from the DB
            List<Status> statuses = _statusRepository.Get();

            if (statuses.Any())
            {
                return Json(statuses);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpPost]
        [Route("/api/status")]
        public ActionResult Add([FromBody]StatusFormModel model)
        {
            // Use the statusrepository to store the status in the DB
            Status status = _statusRepository.Add(model.Name);

            return Json(status);
        }

        [HttpDelete]
        [Route("/api/status")]
        public ActionResult Delete(int id)
        {
            // Use the statusrepository to remove the status from the DB
            Status status = _statusRepository.Delete(id);

            return Json(status);
        }
    }
}
