﻿using BugTracker.Infrastructure.Models;
using BugTracker.Infrastructure.Repositories;
using BugTracker.Web.FormModels;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace BugTracker.Web.Controllers
{
    public class UserApiController : Controller
    {
        public UserApiController(ApplicationDbContext dbContext)
        {
            _userRepository = new UserRepository(dbContext);
        }

        private readonly UserRepository _userRepository;

        [HttpGet]
        [Route("/api/user")]
        public ActionResult Get(int? id)
        {
            // Use the userrepository to retrieve the user from the DB
            List<User> users = _userRepository.Get(id);

            if (users.Any())
            {
                if (id != null)
                {
                    return Json(users.First());
                }
                return Json(users);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpPost]
        [Route("/api/user")]
        public ActionResult Add([FromBody]UserFormModel formModel)
        {
            // Use the userrepository to store the user in the DB
            User user = _userRepository.Add(formModel.Name, formModel.Email);

            return Json(user);
        }

        [HttpDelete]
        [Route("/api/user")]
        public ActionResult Delete(int id)
        {
            // Use the userrepository to remove the user from the DB
            User user = _userRepository.Delete(id);

            return Json(user);
        }

        [HttpPatch]
        [Route("/api/user")]
        public ActionResult Update([FromBody]User userInput)
        {
            // Use the userrepository to update the user in the DB
            User user = _userRepository.Update(userInput);

            if (user != null)
            {
                return Json(user);
            }
            else
            {
                return NotFound();
            }
        }
    }
}
