using BugTracker.Infrastructure.Models;
using BugTracker.Infrastructure.Repositories;
using BugTracker.Test.Helpers;
using BugTracker.Web.Controllers;
using BugTracker.Web.FormModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace BugTracker.Test.RepositoryTests
{
    [TestClass]
    public class BugApiControllerTests : TestBase
    {
        public BugApiControllerTests()
        {
            _statusRepository = new StatusRepository(new ApplicationDbContext(ContextOptions));

            // Add a test status as there is a foreign key constraint requiring it
            _statusRepository.Add("teststatus");
        }

        private readonly StatusRepository _statusRepository;

        [TestMethod]
        public void Get_GetExistingBugs_ShouldReturnBugJson()
        {
            // Add multiple bugs so we have some data to query
            List<Bug> bugs = new List<Bug>
            {
                new Bug
                {
                    Title = "getbug1",
                    Description = "getdesc1",
                    Status = ApplicationDbContext.Statuses.Find(1)
                },
                new Bug
                {
                    Title = "getbug2",
                    Description = "getdesc2",
                    Status = ApplicationDbContext.Statuses.Find(1)
                }
            };
            ApplicationDbContext.Bugs.AddRange(bugs);
            ApplicationDbContext.SaveChangesAsync();

            BugApiController apiController = new BugApiController(ApplicationDbContext);

            // Use wait delegate to wait for the asynchronous database save to complete
            Assert.IsTrue(Wait.For(10, () =>
            {
                // Call the API's get method
                JsonResult result = (JsonResult)apiController.Get(null);

                // Get the test bug from the json
                List<Bug> resultingBugs = result.Value as List<Bug>;

                // Ensure each bug was present in the API response
                foreach (var bug in bugs)
                {
                    Assert.IsTrue(resultingBugs.Any(x => x.Title == bug.Title));
                    Assert.IsTrue(resultingBugs.Any(x => x.Description == bug.Description));
                }
                return true;
            }));
        }

        [TestMethod]
        public void Update_UpdateBug_ShouldUpdateAndReturnBugJson()
        {
            BugApiController apiController = new BugApiController(ApplicationDbContext);
            StatusApiController statusApiController = new StatusApiController(ApplicationDbContext);

            // Add a status first as there's a foreign key constraint requiring it to exist
            statusApiController.Add(new StatusFormModel { Name = "teststatus" });

            // Add a bug first so we can update it
            const string title = "bug1";
            const string description = "desc1";
            const int statusId = 1;
            const int assigneeId = 0;
            JsonResult result = apiController.Add(new BugFormModel
            {
                Title = title,
                Description = description,
                StatusId = statusId,
                AssigneeId = assigneeId
            }) as JsonResult;
            Bug addedBug = result.Value as Bug;

            Bug updatedBug = addedBug;
            const string updatedTitle = "updatedbug1";
            updatedBug.Title = updatedTitle;

            // Update the added bug
            JsonResult updateResult = apiController.Update(updatedBug) as JsonResult;

            // Test that the returned JSON is correct
            Assert.AreEqual(updatedTitle, (result.Value as Bug).Title);

            // Call the API's get method to test that the bug was updated
            JsonResult getResult = apiController.Get(updatedBug.Id) as JsonResult;
            // Test that the bug title has been updated
            Bug resultingBugs = getResult.Value as Bug;
            Assert.AreEqual(resultingBugs.Title, updatedTitle);
        }

        [TestMethod]
        public void Add_AddBug_ShouldSaveAndReturnBugJson()
        {
            const string bugTitle = "addbug1";
            const string bugDescription = "adddesc1";
            BugApiController apiController = new BugApiController(ApplicationDbContext);
            JsonResult result = apiController.Add(new BugFormModel
            {
                Title = bugTitle,
                Description = bugDescription,
                StatusId = 1
            }) as JsonResult;

            // Test that the returned JSON is correct
            Assert.AreEqual(bugTitle, (result.Value as Bug).Title);
            Assert.AreEqual(bugDescription, (result.Value as Bug).Description);

            // Call the API's get method to test that the bug was saved
            JsonResult getResult = (JsonResult)apiController.Get(null);
            List<Bug> resultingBugs = getResult.Value as List<Bug>;
            Assert.IsTrue(resultingBugs.Any(x => x.Title == bugTitle));
            Assert.IsTrue(resultingBugs.Any(x => x.Description == bugDescription));
        }

        [TestMethod]
        public void Delete_DeleteBug_ShouldDeleteAndReturnBugJson()
        {
            BugApiController apiController = new BugApiController(ApplicationDbContext);

            // Add a bug first so we can delete it
            const string bugTitle = "deletebug1";
            const string bugDescription = "deletedesc1";
            JsonResult result = apiController.Add(new BugFormModel
            {
                Title = bugTitle,
                Description = bugDescription,
                StatusId = 1
            }) as JsonResult;
            Bug addedBug = result.Value as Bug;

            // Delete the added bug
            apiController.Delete(addedBug.Id);

            // Call the API's get method to test that the bug was deleted
            ActionResult getResult = apiController.Get(null);
            // Assert that the deleted bug does not exist in the response
            Assert.IsFalse(((getResult as JsonResult).Value as List<Bug>).Any(x => x.Title == bugTitle));
        }
    }
}
