using BugTracker.Infrastructure.Models;
using BugTracker.Test.Helpers;
using BugTracker.Web.Controllers;
using BugTracker.Web.FormModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace BugTracker.Test.RepositoryTests
{
    [TestClass]
    public class UserApiControllerTests : TestBase
    {
        [TestMethod]
        public void Get_GetExistingUsers_ShouldReturnUserJson()
        {
            // Add multiple users so we have some data to query
            List<User> users = new List<User>
            {
                new User { Name = "getuser1", Email = "getuser1@gmail.com" },
                new User { Name = "getuser2", Email = "getuser2@gmail.com" }
            };
            ApplicationDbContext.Users.AddRange(users);
            ApplicationDbContext.SaveChangesAsync();

            UserApiController apiController = new UserApiController(ApplicationDbContext);

            // Use wait delegate to wait for the asynchronous database save to complete
            Assert.IsTrue(Wait.For(10, () =>
            {
                // Call the API's get method
                JsonResult result = (JsonResult)apiController.Get(null);

                // Get the test user from the json
                List<User> resultingUsers = result.Value as List<User>;

                // Ensure each user was present in the API response
                foreach (var user in users)
                {
                    Assert.IsTrue(resultingUsers.Any(x => x.Name == user.Name));
                    Assert.IsTrue(resultingUsers.Any(x => x.Email == user.Email));
                }
                return true;
            }));
        }

        [TestMethod]
        public void Update_UpdateUser_ShouldUpdateAndReturnUserJson()
        {
            UserApiController apiController = new UserApiController(ApplicationDbContext);

            // Add a user first so we can update it
            const string userName = "updateuser1";
            const string userEmail = "updateuser1@gmail.com";
            JsonResult result = apiController.Add(new UserFormModel
            {
                Name = userName,
                Email = userEmail
            }) as JsonResult;
            User addedUser = result.Value as User;

            User updatedUser = addedUser;
            const string updatedUserName = "updateuser2";
            updatedUser.Name = updatedUserName;

            // Update the added user
            JsonResult updateResult = apiController.Update(updatedUser) as JsonResult;

            // Test that the returned JSON is correct
            Assert.AreEqual(updatedUserName, (result.Value as User).Name);

            // Call the API's get method to test that the user was updated
            JsonResult getResult = apiController.Get(null) as JsonResult;
            // Test that the users name has been updated
            List<User> resultingUsers = getResult.Value as List<User>;
            Assert.IsTrue(resultingUsers.Any(x => x.Name == updatedUserName));
        }

        [TestMethod]
        public void Add_AddUser_ShouldSaveAndReturnUserJson()
        {
            const string userName = "adduser1";
            const string userEmail = "adduser1@gmail.com";
            UserApiController apiController = new UserApiController(ApplicationDbContext);
            JsonResult result = apiController.Add(new UserFormModel
            {
                Name = userName,
                Email = userEmail
            }) as JsonResult;

            // Test that the returned JSON is correct
            Assert.AreEqual(userName, (result.Value as User).Name);
            Assert.AreEqual(userEmail, (result.Value as User).Email);

            // Call the API's get method to test that the user was saved
            JsonResult getResult = (JsonResult)apiController.Get(null);
            List<User> resultingUsers = getResult.Value as List<User>;
            Assert.IsTrue(resultingUsers.Any(x => x.Name == userName));
            Assert.IsTrue(resultingUsers.Any(x => x.Email == userEmail));
        }

        [TestMethod]
        public void Delete_DeleteUser_ShouldDeleteAndReturnUserJson()
        {
            UserApiController apiController = new UserApiController(ApplicationDbContext);

            // Add a user first so we can delete it
            const string userName = "deleteuser1";
            const string userEmail = "deleteuser1@gmail.com";
            JsonResult result = apiController.Add(new UserFormModel
            {
                Name = userName,
                Email = userEmail
            }) as JsonResult;
            User addedUser = result.Value as User;

            // Delete the added user
            apiController.Delete(addedUser.Id);

            // Call the API's get method to test that the user was deleted
            ActionResult getResult = apiController.Get(null);
            // Assert that the deleted user does not exist in the response
            Assert.IsFalse(((getResult as JsonResult).Value as List<User>).Any(x => x.Name == userName));
        }
    }
}
