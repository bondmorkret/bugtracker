using BugTracker.Infrastructure.Models;
using BugTracker.Test.Helpers;
using BugTracker.Web.Controllers;
using BugTracker.Web.FormModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace BugTracker.Test.RepositoryTests
{
    [TestClass]
    public class StatusApiControllerTests : TestBase
    {
        [TestMethod]
        public void Get_GetExistingStatuses_ShouldReturnStatusJson()
        {
            // Add multiple status so we have some data to query
            string[] statusNames = new string[] { "getstatus1", "getstatus2" };
            IEnumerable<Status> statuses = statusNames.Select(s => new Status { Name = s });
            ApplicationDbContext.Statuses.AddRange(statuses);
            ApplicationDbContext.SaveChangesAsync();

            StatusApiController apiController = new StatusApiController(ApplicationDbContext);

            // Use wait delegate to wait for the asynchronous database save to complete
            Assert.IsTrue(Wait.For(10, () =>
            {
                // Call the API's get method
                JsonResult result = (JsonResult)apiController.Get();

                // Get the test status from the json
                List<Status> resultingStatuses = result.Value as List<Status>;

                // Ensure each status was present in the API response
                foreach (var status in statuses)
                {
                    Assert.IsTrue(resultingStatuses.Any(x => x.Name == status.Name));
                }
                return true;
            }));
        }

        [TestMethod]
        public void Add_AddStatus_ShouldSaveAndReturnStatusJson()
        {
            const string statusName = "addstatus";
            StatusApiController apiController = new StatusApiController(ApplicationDbContext);
            JsonResult result = apiController.Add(new StatusFormModel
            {
                Name = statusName
            }) as JsonResult;

            // Test that the returned JSON is correct
            Assert.AreEqual(statusName, (result.Value as Status).Name);

            // Call the API's get method to test that the status was saved
            JsonResult getResult = (JsonResult)apiController.Get();
            List<Status> resultingStatuses = getResult.Value as List<Status>;
            Assert.IsTrue(resultingStatuses.Any(x => x.Name == statusName));
        }

        [TestMethod]
        public void Delete_DeleteStatus_ShouldDeleteAndReturnStatusJson()
        {
            StatusApiController apiController = new StatusApiController(ApplicationDbContext);

            // Add a status first so we can delete it
            const string statusName = "deletestatus";
            JsonResult result = apiController.Add(new StatusFormModel
            {
                Name = statusName
            }) as JsonResult;
            Status addedStatus = result.Value as Status;

            // Delete the added status
            apiController.Delete(addedStatus.Id);

            // Call the API's get method to test that the status was deleted
            ActionResult getResult = apiController.Get();
            // Assert that the deleted status does not exist in the response
            Assert.IsFalse(((getResult as JsonResult).Value as List<Status>).Any(x => x.Name == statusName));
        }
    }
}
