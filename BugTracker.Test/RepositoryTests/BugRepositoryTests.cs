﻿using BugTracker.Infrastructure.Models;
using BugTracker.Infrastructure.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace BugTracker.Test.RepositoryTests
{
    [TestClass]
    public class BugRepositoryTests : TestBase
    {
        public BugRepositoryTests()
        {
            _bugRepository = new BugRepository(new ApplicationDbContext(ContextOptions));
            _statusRepository = new StatusRepository(new ApplicationDbContext(ContextOptions));

            // Add a test status as bugs will need to create a foreign key to it
            AddTestStatus();
        }

        private readonly BugRepository _bugRepository;
        private readonly StatusRepository _statusRepository;

        [TestMethod]
        public void Get_GetExistingBugs_ShouldRetrieveBugs()
        {
            // Add multiple bugs so we have some data to query
            IEnumerable<Bug> testBugs = AddTestBugs();

            // Retrieve each bug from the in-memory database
            foreach (var testBug in testBugs)
            {
                Bug foundBug = _bugRepository.Get(testBug.Id)?.FirstOrDefault();

                Assert.IsNotNull(foundBug);
                Assert.AreEqual(testBug.Title, foundBug.Title);
            }
        }

        [TestMethod]
        public void Add_AddNewBug_ShouldSaveViaContext()
        {
            // Add a new bug to the in-memory database
            Bug testBug = AddTestBug();

            // Retrieve it from the in-memory database
            Bug foundBug = _bugRepository.Get(testBug.Id)?.FirstOrDefault();

            Assert.IsNotNull(foundBug);
            Assert.AreEqual(testBug.Title, foundBug.Title);
            Assert.AreEqual(testBug.Description, foundBug.Description);
            Assert.AreEqual(testBug.AssigneeId, foundBug.AssigneeId);
            Assert.AreEqual(testBug.StatusId, foundBug.StatusId);
        }

        [TestMethod]
        public void Delete_DeleteBug_ShouldRemoveViaContext()
        {
            // Add a new bug to the in-memory database
            Bug testBug = AddTestBug();

            // Remove it and attempt to retrieve it from the in-memory database
            _bugRepository.Delete(testBug.Id);
            Bug foundBug = _bugRepository.Get(testBug.Id)?.FirstOrDefault();

            Assert.IsNull(foundBug);
        }

        [TestMethod]
        public void Update_UpdateBug_ShouldUpdateViaContext()
        {
            // Add a new bug to the in-memory database
            Bug testBug = AddTestBug();

            // Make changes to the bug
            Bug updatedBug = testBug;
            updatedBug.Title = "UpdatedTitle";
            updatedBug.Description = "UpdatedDescription";

            // Update it and retrieve it from the in-memory database
            _bugRepository.Update(updatedBug);
            Bug foundBug = _bugRepository.Get(testBug.Id)?.FirstOrDefault();

            Assert.AreEqual(foundBug.Title, updatedBug.Title);
            Assert.AreEqual(foundBug.Description, updatedBug.Description);
        }

        private Bug AddTestBug()
        {
            // Add it to the in-memory database
            return _bugRepository.Add("Bug", "Description", 0, 1);
        }

        private IEnumerable<Bug> AddTestBugs()
        {
            // Create test bug objects
            int[] bugs = new int[] { 1, 2, 3 };
            IEnumerable<Bug> testBugs =
                bugs.Select(bugNumber => _bugRepository.Add($"Bug{bugNumber}",
                                                            $"Description{bugNumber}",
                                                            0,
                                                            1));

            return testBugs;
        }

        private Status AddTestStatus()
        {
            return _statusRepository.Add("teststatus");
        }
    }
}
