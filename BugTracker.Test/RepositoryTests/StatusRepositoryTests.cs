using BugTracker.Infrastructure.Models;
using BugTracker.Infrastructure.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace BugTracker.Test.RepositoryTests
{
    [TestClass]
    public class StatusRepositoryTests : TestBase
    {
        public StatusRepositoryTests()
        {
            _statusRepository = new StatusRepository(new ApplicationDbContext(ContextOptions));
        }

        private readonly StatusRepository _statusRepository;

        [TestMethod]
        public void Get_GetExistingStatuses_ShouldRetrieveStatuses()
        {
            // Add multiple status so we have some data to query
            IEnumerable<Status> testStatuses = AddTestStatuses();

            // Retrieve each status from the in-memory database
            foreach (var testStatus in testStatuses)
            {
                Status foundStatus = _statusRepository
                    .Get()
                    .First(status => status.Id == testStatus.Id);

                Assert.IsNotNull(foundStatus);
                Assert.AreEqual(testStatus.Name, foundStatus.Name);
            }
        }

        [TestMethod]
        public void Add_AddNewStatus_ShouldSaveViaContext()
        {
            // Add a new status to the in-memory database
            Status testStatus = AddTestStatus();

            // Retrieve it from the in-memory database
            Status foundStatus = _statusRepository
                .Get()
                .First(status => status.Id == testStatus.Id);

            Assert.IsNotNull(foundStatus);
            Assert.AreEqual(testStatus.Name, foundStatus.Name);
        }

        [TestMethod]
        public void Delete_DeleteStatus_ShouldRemoveViaContext()
        {
            // Add a new status to the in-memory database
            Status testStatus = AddTestStatus();

            // Remove it and attempt to retrieve it from the in-memory database
            _statusRepository.Delete(testStatus.Id);
            Status foundStatus = _statusRepository
                .Get()
                .FirstOrDefault(status => status.Id == testStatus.Id);

            Assert.IsNull(foundStatus);
        }

        private Status AddTestStatus()
        {
            // Add a status to the in-memory database
            return _statusRepository.Add("TestStatus");
        }

        private IEnumerable<Status> AddTestStatuses()
        {
            // Create test status objects
            string[] statuses = new string[] { "test1", "test2", "test3" };
            IEnumerable<Status> testStatuses =
                statuses.Select(status => _statusRepository.Add(status));

            return testStatuses;
        }
    }
}
