﻿using BugTracker.Infrastructure.Models;
using BugTracker.Infrastructure.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace BugTracker.Test.RepositoryTests
{
    [TestClass]
    public class UserRepositoryTests : TestBase
    {
        public UserRepositoryTests()
        {
            _userRepository = new UserRepository(new ApplicationDbContext(ContextOptions));
        }

        private readonly UserRepository _userRepository;

        [TestMethod]
        public void Get_GetExistingUsers_ShouldRetrieveUsers()
        {
            // Add multiple users so we have some data to query
            IEnumerable<User> testUsers = AddTestUsers();

            // Retrieve each user from the in-memory database
            foreach (var testUser in testUsers)
            {
                User foundUser = _userRepository.Get(testUser.Id)?.FirstOrDefault();

                Assert.IsNotNull(foundUser);
                Assert.AreEqual(testUser.Name, foundUser.Name);
            }
        }

        [TestMethod]
        public void Add_AddNewUser_ShouldSaveViaContext()
        {
            // Add a new user to the in-memory database
            User testUser = AddTestUser();

            // Retrieve it from the in-memory database
            User foundUser = _userRepository.Get(testUser.Id)?.FirstOrDefault();

            Assert.IsNotNull(foundUser);
            Assert.AreEqual(testUser.Name, foundUser.Name);
            Assert.AreEqual(testUser.Email, foundUser.Email);;
        }

        [TestMethod]
        public void Delete_DeleteUser_ShouldRemoveViaContext()
        {
            // Add a new user to the in-memory database
            User testUser = AddTestUser();

            // Remove it and attempt to retrieve it from the in-memory database
            _userRepository.Delete(testUser.Id);
            User foundUser = _userRepository.Get(testUser.Id)?.FirstOrDefault();

            Assert.IsNull(foundUser);
        }

        [TestMethod]
        public void Update_UpdateUser_ShouldUpdateViaContext()
        {
            // Add a new user to the in-memory database
            User testUser = AddTestUser();

            // Make changes to the user
            User updatedUser = testUser;
            updatedUser.Name = "UpdatedName";
            updatedUser.Email = "UpdatedEmail";

            // Update it and retrieve it from the in-memory database
            _userRepository.Update(updatedUser);
            User foundUser = _userRepository.Get(testUser.Id)?.FirstOrDefault();

            Assert.AreEqual(foundUser.Name, updatedUser.Name);
            Assert.AreEqual(foundUser.Email, updatedUser.Email);
        }

        private User AddTestUser()
        {
            // Add it to the in-memory database
            return _userRepository.Add("name", "email@gmail.com");
        }

        private IEnumerable<User> AddTestUsers()
        {
            // Create test user objects
            int[] users = new int[] { 1, 2, 3 };
            IEnumerable<User> testUsers =
                users.Select(userNumber => _userRepository.Add($"User{userNumber}",
                                                            $"Email{userNumber}@gmail.com"));

            return testUsers;
        }
    }
}
