﻿using System;
using System.Threading;

namespace BugTracker.Test.Helpers
{
    public class Wait
    {
        public static bool For(int timeoutSeconds, Func<bool> assertion)
        {
            DateTime start = DateTime.Now;
            bool result = false;

            while (DateTime.Now - start < TimeSpan.FromSeconds(timeoutSeconds)
                && result == false)
            {
                try
                {
                    result = assertion();
                }
                catch (Exception ex)
                {
                    // Catch the exception because we're still within the timeout
                    continue;
                }
                Thread.Sleep(1000);
            }

            return result;
        }
    }
}
