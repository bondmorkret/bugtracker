﻿using BugTracker.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;

namespace BugTracker.Test.RepositoryTests
{
    public class TestBase
    {
        public TestBase()
        {
            ContextOptions = NewContextOptions();
            ApplicationDbContext = NewContext();
        }

        protected DbContextOptions<ApplicationDbContext> ContextOptions;
        protected ApplicationDbContext ApplicationDbContext;

        protected DbContextOptions<ApplicationDbContext> NewContextOptions()
        {
            var builder = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase("BugTrackerDb")
                .EnableSensitiveDataLogging();

            return builder.Options;
        }

        protected ApplicationDbContext NewContext()
        {
            return new ApplicationDbContext(ContextOptions);
        }
    }
}
